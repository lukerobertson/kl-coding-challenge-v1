import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { observable, action } from 'mobx'

import style from './Main.scss'

@inject('global')
@observer
class Main extends React.Component {

    @action
    onClick = () => {


    }

    @observable playerOne = 0

    @observable playerTwo = 0

    @action
    playerOneScore = () => {
        this.playerOne = this.playerOne + 1
    }

    @action
    playerTwoScore = () => {
        this.playerTwo = this.playerTwo + 1
    }

    @observable score = ['','','','','','','','','']

    @action
    resetScore = () => {
        this.score = ['','','','','','','','','']
    }

    cpuScore = () => {
    }

    checkScores = () => {
    }

    render() {
        return (
            <div className="main-container">

                <div className="main-title">

                    <div className="main-title__text">
                        Player 1
                        <div className="main-title__score">{this.playerOne}</div>
                    </div>
                    <div className="main-title__text">
                        CPU
                        <div className="main-title__score">{this.playerTwo}</div>
                    </div>

                </div>

                <div className="main-pitch__container">

                    <div className="main-pitch__visual-layout">
                        <div className="main-pitch__visual-v"><div /><div /></div>
                        <div className="main-pitch__visual-h"><div /><div /></div>
                    </div>

                    <div className="main-pitch__board">

                        {this.score.map((item, index) => 
                            <div key={index} className="main-pitch__board-row" onClick={() => this.onClick()}>{item}</div>)}

                    </div>

                </div>

            </div>
        )
    }
}

export default Main
