import React from 'react'

import { Router, Route, Switch } from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'
import { RouterStore, syncHistoryWithStore } from 'mobx-react-router'
import { Provider } from 'mobx-react'

import store from './stores'

import App from './App'

import Main from './components/Main/Main'

const browserHistory = createBrowserHistory()
const routingStore = new RouterStore()

const stores = {
    routing: routingStore,
    global: store
}

const history = syncHistoryWithStore(browserHistory, routingStore)

const Routes = () => {
    return (
        <Provider {...stores}>
            <Router history={history}>
                <App>
                    <Switch>
                        <Route exact path="/" component={Main} />
                    </Switch>
                </App>
            </Router>
        </Provider>
    )
}

export default Routes
