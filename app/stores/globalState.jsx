import { observable, action } from 'mobx'

class GlobalState {
    @observable token = false
    @observable user = {}

    @action
    storeUser = user => {
        this.user = user
    }

    @action
    storeToken = token => {
        this.token = token
    }

    @action
    deleteToken = token => {
        this.token = false
    }
}

const store = new GlobalState()

export default store
